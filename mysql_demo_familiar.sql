--
-- Estructura de tabla para la tabla `familiar`
--

CREATE TABLE IF NOT EXISTS `familiar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `parentesco` int(11) NOT NULL,
  `nombre_fam` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `edad_fam` int(11) NOT NULL,
  `telefono` varchar(11) NOT NULL,
  `ocupacion` int(11) NOT NULL,
  `instruccion_fam` int(11) NOT NULL,
  `vive` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ocupacion_ocupacion` (`ocupacion`),
  KEY `fk_grado_instruccion_instruccion_fam` (`instruccion_fam`),
  KEY `fk_parentesco_parentesco` (`parentesco`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `familiar`
--

INSERT INTO `familiar` (`id`, `cedula`, `parentesco`, `nombre_fam`, `edad_fam`, `telefono`, `ocupacion`, `instruccion_fam`, `vive`) VALUES
(1, 13571097, 11, 'Carmen', 60, '04243095689', 9, 3, 'Si'),
(2, 13571097, 15, 'José', 81, '04245689702', 194, 4, 'Si'),
(4, 13598789, 11, 'Rosa Pérez', 48, '04245697896', 9, 2, 'Si'),
(5, 19787661, 11, 'maria', 52, '02345147736', 2, 1, 'Si');


