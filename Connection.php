<?php

/**
 * @Property: GPL
 * @Author: regorio J Bolívar B
 * @email: elalconxvii@gmail.com
 * @Creation Date: 08/01/2014
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 09/01/2014
 * @Description: Codigo de configuracion a base de datos.
 * @package: Connection.php
 * @version: 0.1
 */
class Connection {
    var $host = array('localhost');
    var $port = array('3228');
    var $dbna = array('demo');
    var $user = array('root');
    var $pass = array('123456');
    var $driver = array('mysql');
    var $index;
    var $conn;      // Identificador de la conexion
    var $result;    // Resultado del query
    var $numrow;    // Numeros de registros asociados
    var $allrow;    // Todos los registros en un array

    public function __construct($index) {
        $this->host = $this->host[$index];
        $this->driver = $this->driver[$index];
        $this->dbna = $this->dbna[$index];
        $this->port = $this->port[$index];
        $this->user = $this->user[$index];
        $this->pass = $this->pass[$index];
        return $this->conn = $this->conexion($index);
    }

    public function conexion( ) {
        try {
            switch ($this->driver) {
                case 'postgresql':
                    $this->conn = @pg_connect("host=" . $this->host . " port=" . $this->port . " dbname=" . $this->dbna . " user=" . $this->user . " password=" . $this->pass . "");
                    break;
                case 'mysql':
                    $this->conn = mysql_connect($this->host, $this->user, $this->pass) or mysql_error();
                    mysql_select_db($this->dbna, $this->conn);
                    break;
            }
            /** Verificar que la variable conexion si es nula controle el error */
            if (!$this->conn) {
                die('NO se pudo establecer la conexion al Servidor');
            }
        } catch (Exception $value) {
            echo "Defaul Exception $value";
        }
        return $this->conn;
    }

    public function execute($sql) {
        switch ($this->driver) {
            case 'postgresql':
                $this->result = pg_query($this->conn, $sql);
                break;
            case 'mysql':
                $this->result = mysql_query($sql, $this->conn);
                break;
            default:
                die('Hay un error al executar el query');
                break;
        }
        return $this->result;
    }

    public function num_row() {
        switch ($this->driver) {
            case 'postgresql':
                $this->numrow = @pg_num_rows($this->result);
                break;
            case 'mysql':
                $this->numrow = mysql_num_rows($this->result);
                break;
        }
        return $this->numrow;
    }

    public function all_row() {
        switch ($this->driver) {
            case 'postgresql':
                $this->allrow = pg_fetch_all($this->result);
                break;
            case 'mysql':
                $datoJson = NULL;
                $valor = 1;
                while ($rows = mysql_fetch_array($this->result, MYSQL_ASSOC)) {
                    foreach ($rows as $key => $value):
                        $datoJson[$valor][$key] = $value;
                    endforeach;
                    $valor++;
                }
                $this->allrow = $datoJson;
                break;
        }
        return $this->allrow;
    }

    public function readSchema() {
        switch ($this->driver) {
            case 'postgresql':
                pg_close($this->conn);
                break;
            case 'mysql':
                $sql = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_SCHEMA='" . $this->dbna . "' ORDER BY table_name DESC";
                self::execute($sql);
                return self::all_row();
                self::close();
                break;
        }
    }

    public function close() {
        switch ($this->driver) {
            case 'postgresql':
                pg_close($this->conn);
                break;
            case 'mysql':
                mysql_close($this->conn);
                break;
        }
    }

}

