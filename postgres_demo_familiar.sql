CREATE DATABASE demo
  WITH ENCODING='UTF8'
       CONNECTION LIMIT=-1;
--
-- Estructura de tabla para la tabla familiar
--

CREATE TABLE familiar (
  id serial NOT NULL,
  cedula integer NOT NULL,
  parentesco integer NOT NULL,
  nombre_fam varchar(100),
  edad_fam integer NOT NULL,
  telefono varchar(11) NOT NULL,
  ocupacion integer NOT NULL,
  instruccion_fam integer NOT NULL,
  vive varchar(2) NOT NULL,
  PRIMARY KEY (id)
);

--
-- Volcado de datos para la tabla familiar
--

INSERT INTO familiar (id, cedula, parentesco, nombre_fam, edad_fam, telefono, ocupacion, instruccion_fam, vive) VALUES
(1, 13571097, 11, 'Carmen', 60, '04243095689', 9, 3, 'Si'),
(2, 13571097, 15, 'José', 81, '04245689702', 194, 4, 'Si'),
(4, 13598789, 11, 'Rosa Pérez', 48, '04245697896', 9, 2, 'Si'),
(5, 19787661, 11, 'maria', 52, '02345147736', 2, 1, 'Si');


