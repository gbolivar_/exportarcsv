<?php
/**
 * @Property: GPL
 * @Author: regorio J Bolívar B
 * @email: elalconxvii@gmail.com
 * @Creation Date: 13/03/2014
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 13/03/2014
 * @Description: Exposrtar data a scv.
 * @package: exportarCSV.php
 * @version: 0.1
 */
include_once('Connection.php');
$conn = NEW Connection(0);
$sql1 = 'SET NAMES utf8';
$sql2 = "SELECT id, cedula, parentesco, nombre_fam, edad_fam, telefono, ocupacion, instruccion_fam, vive  FROM familiar"; 
$result = $conn->execute($sql1);
$result2 = $conn->execute($sql2);

/* Cabecera del archivo CSV */
$lista = array (array('id', 'cedula', 'parentesco', 'nombre_fam','edad_fam', 'telefono','ocupacion','instruccion_fam','vive'));
/* 
Array
(
    [id] => 1
    [cedula] => 13571097
    [parentesco] => 11
    [nombre_fam] => Carmen
    [edad_fam] => 60
    [telefono] => 04243095689
    [ocupacion] => 9
    [instruccion_fam] => 3
    [vive] => Si
)
*/
/* Crear los registro que seran agregago al csv */
$data = $conn->all_row($result2);
foreach ($data as $rows) {
	array_push($lista, array($rows['id'], $rows['cedula'], $rows['parentesco'], $rows['nombre_fam'], $rows['edad_fam'], $rows['telefono'], $rows['ocupacion'], $rows['instruccion_fam'], $rows['vive']));
}
/* Procedimientopara crear el csv */
$fp = fopen('file.csv', 'w');
foreach ($lista as $campos) {
	fputcsv($fp, $campos);
}
fclose($fp);
?>
<a href="file.csv">Descargar</a>